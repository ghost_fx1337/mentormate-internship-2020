class Grid {
  // Relative coordinates of surrounding cells
  private static surroundingCells: any = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1],
  ];
  private _cells: number[][] = [];

  constructor(private rows: number, private columns: number, lines: string[]) {
    lines.forEach((line) => {
      this._cells.push(line.split("").map(Number));
    });
    // console.log(this._cells);
  }

  // Updates the generation according to the task requirements:
  nextGeneration(): void {
    const nextGenCells: number[][] = [];
    this._cells.forEach((row, rowIndex) => {
      nextGenCells.push([]);
      row.forEach((cell, colIndex) => {
        let greens = 0;
        Grid.surroundingCells.forEach((surroundingCell) => {
          let x = colIndex + surroundingCell[0];
          let y = rowIndex + surroundingCell[1];

          // Check if out of bounds 
          if (x < 0 || x >= this.columns || y < 0 || y >= this.rows) {
            return;
          }

          // Increent greens counter if found surrounding green cell
          if (this._cells[y][x] === 1) {
            greens++;
          }
        });

        // Set default color to red
        let nextGameCellColor = 0;
        if (cell === 1) {
          if (greens === 2 || greens === 3 || greens === 6) {
            nextGameCellColor = 1;
          }
        } 
        // If cell is red
        else {
          if (greens === 3 || greens === 6) {
            nextGameCellColor = 1;
          }
        }

        // Set the new value of next generation per cell
        nextGenCells[rowIndex].push(nextGameCellColor);
      });
    });
    this._cells = nextGenCells;
    console.log(this._cells);
  }

  // Check ifcell on exact coordinates is green or red
  isCellGreen(x, y): boolean {
    return this._cells[y][x] === 1;
  }
}

class Game {
    // Start new game
  play(
    rows: number,
    columns: number,
    lines: string[],
    x: number,
    y: number,
    turns: number
  ): number {

    // Create Generation Zero
    const grid = new Grid(rows, columns, lines);

    // If cell with x1, y1 coordinates is green, increment counter with 1
    let greenCounter = grid.isCellGreen(x, y) ? 1 : 0;

    // Create new Generation for each turn
    for (let i = 0; i < turns ; i++) {
      grid.nextGeneration();
      if (grid.isCellGreen(x, y)) {
        greenCounter++;
      }
    }
    console.log(greenCounter);
    return greenCounter;
  }
}

// TESTING SECTION:
const rows = 3;
const columns = 3;
const lines = ["000", "111", "000"];
const x = 1;
const y = 0;
const turns = 10;

const game = new Game();

game.play(rows, columns, lines, x, y, turns);
