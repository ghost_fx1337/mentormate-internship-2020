Language used: **TypeScript**
<br>
OOP: **Yes**
<br>
Comments: **Yes**
<br>
Good Naming Convention: I hope so
<br>
Documentation: **Yes**
<br>

*NODE REQUIRED*:

**brew install node (MacOS)**
**https://nodejs.org/en/download/** **(Windows)** 

*TS-NODE REQUIRED*:

**npm i ts-node**

You need to run **ts-node** **FILENAME** to execute the algorithm. 

If you want to populate new data for another test case you can find the **TESTING SECTION** in the file where new parameters can be updated.
